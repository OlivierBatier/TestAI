import random

import math
import networkx as nx
import copy

from Neurone import Neurone


class Genome:
    genomeId = 0

    def __init__(self, inputs):
        self.neurones = []
        self.innovationNumber = 0
        self.numberOfNeurones = 0
        self.id = Genome.genomeId
        Genome.genomeId += 1
        # restrain the size of the hidden layers
        self.maxHiddenNode = 7
        self.fitness = 0
        for input in inputs:
            self.addNeurone(type="INPUT", name=input)
            self.maxHiddenNode += 1
        self.addNeurone(type="OUTPUT", name="OUTPUT")
        self.maxHiddenNode += 1

    def addNeurone(self, type="NEURONE", name=None):
        Neurone(self, type=type, name=name)

    def printGenome(self):
        print("GENOME: " + str(self.id))
        for neurone in self.neurones:
            print("SIZE: " + str(len(self.neurones)))
            neurone.printNeurone()
        self.reinit()

    def reinit(self):
        for neurone in self.neurones:
            neurone.isChecked = False

    """
        rebuilds connections between neurones after crossover
    """

    def remakeConnections(self):
        for neurone in self.neurones:
            for connection in reversed(neurone.outputConnection):
                isInNeurones = next(
                    (x for x in self.neurones if
                     (connection is not None and x.neuroneId == connection.output.neuroneId)),
                    "NONE")
                # check if the connection endpoint exists
                if isInNeurones != "NONE":
                    connection.output = isInNeurones

    """
        crossover between two genomes
    """

    def crossover(self, otherGenome):
        maxInnovationNumber = max(self.innovationNumber, otherGenome.innovationNumber)

        # identify the stronger and the weaker genome
        strongerGenome = (self.neurones if (self.fitness > otherGenome.fitness) else otherGenome.neurones)
        weakerGenome = (self.neurones if (self.fitness <= otherGenome.fitness) else otherGenome.neurones)

        # if a neurone doesn't exist in the stronger genome but exists in the weaker one we add it
        for neurone in weakerGenome:
            neurone1 = next((x for x in strongerGenome if (x.neuroneId == neurone.neuroneId)),
                            "NONE")
            if neurone1 == "NONE":
                strongerGenome.append(neurone)

        for neurone in strongerGenome:
            if neurone.type != "OUTPUT":
                neurone1 = next((x for x in weakerGenome if (x.neuroneId == neurone.neuroneId)),
                                "NONE")
                # if a neurone exists in both genomes
                if neurone1 != "NONE" and neurone1.outputConnection != [None]:
                    for gene in neurone1.outputConnection:
                        gene1 = next((x for x in neurone.outputConnection if (
                            (x is not None and gene is not None) and x.output.neuroneId == gene.output.neuroneId)),
                                     "NONE")
                        # if a gene doesn't exist in the stronger genome we add it
                        if gene1 == "NONE" and gene is not None:
                            neurone.outputConnection.append(gene)

        # copy the stronger genome to the current genome
        self.neurones = copy.deepcopy(strongerGenome)

        # Set the new values
        self.numberOfNeurones = len(self.neurones)
        self.innovationNumber = maxInnovationNumber
        self.remakeConnections()

    """
        Mutates the genome 
    """

    def mutate(self):
        self.id = Genome.genomeId
        Genome.genomeId += 1
        chosenNeuroneType = "OUTPUT"
        neurone = None
        # choses randomly the neurone to mutate
        while chosenNeuroneType == "OUTPUT":
            neurone = self.neurones[random.randint(0, len(self.neurones) - 1)]
            chosenNeuroneType = neurone.type
        mutationToDo = random.random()
        # If the neurone genes
        if mutationToDo < 0.85 and neurone.outputConnection != [None]:
            chosenConnection = None
            tries = 0
            # chooses a gene to apply the mutation to
            while chosenConnection is None and tries < 10:
                tries += 1
                chosenConnection = neurone.outputConnection[random.randint(0, len(neurone.outputConnection) - 1)]
            # applies the mutation to the gene if it's enabled
            if chosenConnection is not None and chosenConnection.enabled:
                if mutationToDo < 0.1:
                    chosenConnection.enableDisableMutate()
                elif mutationToDo < 0.82:
                    chosenConnection.pointMutate()
                elif mutationToDo < 0.85 and len(self.neurones) < self.maxHiddenNode:
                    chosenConnection.neuroneMutate()
                else:
                    # if the number of max neurones is reached the retry another mutation
                    self.mutate()
            elif chosenConnection is not None:
                # else enable it
                if mutationToDo < 0.1:
                    chosenConnection.enableDisableMutate()
                else:
                    self.mutate()
        # else
        else:
            connectionPossible = False
            tries = 0
            # Finda possible gene to create
            while connectionPossible is False and tries < 10:
                tries += 1
                index = random.randint(0, len(self.neurones) - 1)
                connectionPossible = self.neurones[index] != neurone and self.neurones[index].type != "INPUT"

            if connectionPossible:
                for connection in neurone.outputConnection:
                    if connection is not None and connection.output == self.neurones[index]:
                        connectionPossible = False
                        break
            if connectionPossible:
                neurone.linkMutate(self.neurones[index])

    """
        sends the information to the neural network in order to take a decision
    """

    def feedForward(self, gameStatus):
        # resets the output value
        for neurone in self.neurones:
            if neurone.type == "OUTPUT":
                neurone.value = 0
                outputNeurone = neurone

        # send each information to each neurone
        for inputInfo in gameStatus:
            for neurone in self.neurones:
                if neurone.name == inputInfo:
                    self.reinit()
                    neurone.feedForward(gameStatus[inputInfo] / 10)

        return 1 if outputNeurone.value < 0.5 else 0

    """
        create a representation of the genome in term of paths
    """

    def getTopology(self):
        topology = []

        for neurone in self.neurones:
            if neurone.type == "INPUT":
                self.reinit()
                infoPath = []
                neurone.getTopology(infoPath, topology)

        return topology

    """
        check similarity of two genomes
    """

    def isSimilarTo(self, genome2):
        # get the topologies of the two genomes
        topology1 = self.getTopology()
        topology2 = genome2.getTopology()
        totalDiff = 0
        for path1 in topology1:
            minDiff = 100
            for path2 in topology2:
                # get two paths with the same inputs and outputs
                if path1[0] == path2[0] and path1[len(path1) - 1] == path2[len(path2) - 1]:
                    distancePath1 = 0
                    distancePath2 = 0
                    for i in range(1, len(path1) - 1):
                        distancePath1 += path1[i]
                    for i in range(1, len(path2) - 1):
                        distancePath2 += path2[i]
                    distancePath1 += len(path1) * 0.5
                    distancePath2 += len(path2) * 0.5
                    # get the percentage of difference between the two paths
                    diff = abs(1 - (distancePath1 / distancePath2)) * 100
                else:
                    diff = 20
                # get the minimal difference for each path
                if minDiff > diff:
                    minDiff = diff
            totalDiff += minDiff
            # check if the % is inferior to 5
        return totalDiff / len(topology1) < 5

    """
        creates the visual graph
    """

    def createGraph(self):
        graph = nx.DiGraph()
        for neurone in self.neurones:
            for connection in neurone.outputConnection:
                if connection is not None and connection.enabled:
                    if neurone.type == "INPUT":
                        graph.add_edge(neurone.neuroneId, connection.output.neuroneId, color="blue")
                    elif neurone.type == "OUTPUT":
                        graph.add_edge(neurone.neuroneId, connection.output.neuroneId, color="green")
                    else:
                        graph.add_edge(neurone.neuroneId, connection.output.neuroneId, color="red")
                else:
                    if neurone.type == "INPUT":
                        graph.add_node(neurone.neuroneId, color="blue")
                    elif neurone.type == "OUTPUT":
                        graph.add_node(neurone.neuroneId, color="green")
                    else:
                        graph.add_node(neurone.neuroneId, color="red")
        return graph
