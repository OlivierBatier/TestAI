import time
import random

import copy

import math
import networkx as nx
import matplotlib.pyplot as plt
import atexit
import jsonpickle
import jsonpickle.ext.numpy as jsonpickle_numpy
import sys

from Genome import Genome
from Species import Species
from MyAgent import MyAgent

from ple.games.flappybird import FlappyBird
from ple import PLE
from time import gmtime, strftime

"""
draws the graph of the genome
blue dots represent inputs
red dots represent hidden neurones
green dot is the output
"""


def drawGraph(genomeToDraw, id):
    plt.clf()
    plt.axis('off')
    graph = genomeToDraw.createGraph()

    node_color = []
    for node in graph.nodes(data=True):
        if len(node) == 2 and "color" in node[1]:
            node_color.append(node[1]['color'])
        else:
            node_color.append("red")

    nx.draw_networkx(graph, node_color=node_color)
    genomeName = strftime("Genome_") + str(id) + ".png"
    plt.savefig("./Genomes/" + genomeName)


"""
regroup all the genomes in species by comparing them to each other
"""


def identifySpecies(speciesList):
    speciesIndx = 0
    # we go through the list of species
    while speciesIndx < len(speciesList):
        genome1Indx = 0
        # we go through the list of genomes for each species
        while genome1Indx < len(speciesList[speciesIndx].genomes):
            species2Indx = speciesIndx
            genome1 = speciesList[speciesIndx].genomes[genome1Indx]
            # we compare each genome to all the following genomes
            while species2Indx < len(speciesList):
                if speciesIndx == species2Indx:
                    genome2Indx = genome1Indx + 1
                else:
                    genome2Indx = 0
                while genome2Indx < len(speciesList[species2Indx].genomes):
                    genome2 = speciesList[species2Indx].genomes[genome2Indx]
                    # if one of the genomes selected has no Species we create one
                    if genome1.speciesId is None:
                        newSpecies = Species()
                        newSpecies.addGenome(speciesList[speciesIndx].genomes[genome1Indx])
                        speciesList.append(newSpecies)
                    elif genome2.speciesId is None:
                        newSpecies = Species()
                        newSpecies.addGenome(speciesList[species2Indx].genomes[genome2Indx])
                        speciesList.append(newSpecies)
                        # if the sfirst genome is similar to the second one and they are from two different species we move the first genome to the second species
                    elif genome1.isSimilarTo(genome2) and speciesIndx != species2Indx:
                        genomeToMove = speciesList[species2Indx].genomes.pop(genome2Indx)
                        speciesList[speciesIndx].addGenome(genomeToMove)
                        # if they are not similar but from the same species we move the second genome to a new species
                    elif not genome1.isSimilarTo(genome2) and speciesIndx == species2Indx:
                        newSpecies = Species()
                        genomeToMove = speciesList[species2Indx].genomes.pop(genome2Indx)
                        newSpecies.addGenome(genomeToMove)
                        speciesList.append(newSpecies)
                    genome2Indx += 1
                species2Indx += 1
            genome1Indx += 1
        speciesIndx += 1
    return speciesList


"""
readapt the pool of genomes
"""


def adaptPool(speciesList, poolSize, totalFitness, previousFitness):
    # Assigne each genome to a species
    speciesList = identifySpecies(speciesList);

    allGenomePool = []
    probabilityOfMutation = 0.9 if totalFitness > previousFitness else 0.9

    # for each species we kill the lower half of the genomes
    for species in speciesList:
        speciesSize = len(species.genomes)
        if speciesSize == 0:
            continue
        species.genomes.sort(key=lambda x: x.fitness, reverse=True)
        species.genomes = species.genomes[0:int(speciesSize / 2) + 1]
        # constitute the new genomePool with the survivor of each species
        for survivor in species.genomes:
            allGenomePool.append(copy.deepcopy(survivor))
        species.genomes = []

    allGenomePool.sort(key=lambda x: x.fitness, reverse=True)

    weights = []

    for survivor in allGenomePool:
        normalizedWeight = survivor.fitness - allGenomePool[- 1].fitness
        weights.append(normalizedWeight)

    # size of the genome pool we want to keep for the next generation
    numberOfGenomesToKeep = int(len(allGenomePool) / 2)
    newGenomePool = allGenomePool[0:numberOfGenomesToKeep]
    # reconstruct a genomePool with a weighted distribution
    newGenomePool += random.choices(population=allGenomePool, weights=weights, k=poolSize - len(newGenomePool))

    # mutation or crossover of the added genomes
    indxGenome = 0
    while indxGenome < len(newGenomePool):
        tempgenome = newGenomePool[indxGenome]
        for species in speciesList:
            if tempgenome.speciesId == species.id:
                # If it's not one of the untouchable genomes randomly mutate or crossover it
                if indxGenome > numberOfGenomesToKeep - 1:
                    # mutate genome or crossover randomly
                    if random.random() < probabilityOfMutation:
                        tempgenome.mutate()
                    else:
                        tempgenome.crossover(newGenomePool[random.randint(0, numberOfGenomesToKeep)])
                species.addGenome(tempgenome)
        indxGenome += 1

    # delete empty species
    for index in reversed(range(len(speciesList))):
        if len(speciesList[index].genomes) == 0:
            speciesList.pop(index)

    return speciesList


"""
simplifies the input from PLE to help a little the computation
"""


def simplifyInput(listOfInputsComplicated):
    simplifiedInput = []
    for index in range(0, 5):
        if index != 1:
            simplifiedInput.append(listOfInputsComplicated[index])
    return simplifiedInput


"""
    initialises the genome pool
"""


def initSpeciesPool(size):
    newSpeciesPool = []
    newSpecies = Species()
    for i in range(0, size):
        genome = Genome(simplifyInput(list(game.getGameState().keys())))
        newSpecies.addGenome(genome)
    newSpeciesPool.append(newSpecies)
    return newSpeciesPool


"""
    exports the species list to a file to be reused later on
"""


@atexit.register
def exportList():
    with open('species.txt', 'w') as f:
        f.truncate()
        f.write(jsonpickle.encode(speciesList))


jsonpickle_numpy.register_handlers()
# initiliasing game, fps should not be touched but display_screen can help on slow computers
game = FlappyBird()
p = PLE(game, fps=30, display_screen=True)
agent = MyAgent(allowed_actions=p.getActionSet())
# initilising variables and inputs
p.init()
reward = 0.0
beginTime = 0.0
poolSize = 80
nbOfTries = 5000
reinitSpecies = False
saveSpecies = False
args = sys.argv

# args from command line
for i in range(len(args)):
    if args[i] == "reinit":
        reinitSpecies = True
    if args[i] == "save":
        saveSpecies = True
    if args[i] == "poolSize" and i < len(args) - 1:
        poolSize = int(args[i + 1])
maxFitness = -100000

distFromPipeHole = 0
speciesList = initSpeciesPool(poolSize)

if not reinitSpecies:
    with open('species.txt') as data_file:
        try:
            speciesList = jsonpickle.decode(data_file.read())
        except ValueError:
            speciesList = initSpeciesPool(poolSize)

totalFitness = 1
# game loop
while nbOfTries != 0:
    previousFitness = totalFitness
    totalFitness = 0
    indexToPrint = 0
    listHist = []
    # run the game for each genome
    for species in speciesList:
        for genome in species.genomes:

            agent.currentGenome = genome
            reward = 0
            energy = 0
            beginTime = time.time()
            while True:
                if p.game_over():
                    endGameStatus = game.getGameState()
                    reward = reward + 5000 - 3 * distFromPipeHole - 1.5 * energy + (time.time() - beginTime)
                    if reward > maxFitness:
                        maxFitness = reward
                    totalFitness += reward
                    genome.fitness = reward
                    p.reset_game()
                    drawGraph(genome, indexToPrint)
                    indexToPrint += 1
                    # genome.printGenome()

                    # f = open('workfile.txt', 'w')
                    # f.write(str(reward))
                    # f.write("\n")
                    # f.close()
                    break

                observation = p.getScreenRGB()
                action = agent.pickAction(reward, game.getGameState())
                if action == agent.actions[0]:
                    energy += 1

                distFromPipeHole = math.sqrt((game.getGameState()["player_y"] - (
                    game.getGameState()["next_pipe_bottom_y"] + game.getGameState()["next_pipe_top_y"]) / 2) ** 2)
                # defines the reward of the action (+1000 foir each pipe passed)
                reward += p.act(action) * 1000
                # time.sleep(.25)
        listHist.append(len(species.genomes))

    print("Generation", 5000 - nbOfTries, "Total Fitness: ", totalFitness)
    print("Numb of Species: " + str(len(speciesList)))
    print("SPECIES", listHist)
    print("Max Fitness", maxFitness)

    nbOfTries -= 1
    if saveSpecies:
        exportList()
    speciesList = adaptPool(speciesList, poolSize, totalFitness, previousFitness)
