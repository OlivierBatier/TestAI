import math


class MyAgent:
    def __init__(self, allowed_actions):
        self.actions = allowed_actions
        self.currentGenome = None

    # picks the action is supposed to do
    def pickAction(self, reward, gameStatus):
        actionToDo = self.currentGenome.feedForward(gameStatus)
        if actionToDo is not None:
            return self.actions[actionToDo]
        else:
            return self.actions[1]


def sigmoid(gamma):
    if gamma < 0:
        return 1 - 1 / (1 + math.exp(gamma))
    return 1 / (1 + math.exp(-gamma))
