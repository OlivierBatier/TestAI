import random


class Neurone:
    def __init__(self, genome, outputConnection=None, type="NEURONE", name=None):
        self.outputConnection = [outputConnection]
        self.type = type
        self.name = name
        self.neuroneId = genome.numberOfNeurones
        self.value = 0
        self.isChecked = False
        self.genome = genome
        self.genome.neurones.append(self)
        self.genome.numberOfNeurones += 1
        self.inputs = []
        self.inputSize = 0

    """
        Randomly add a new connection
    """

    def linkMutate(self, neurone):
        newConnection = Connection(neurone, neurone.genome)

        self.outputConnection.append(newConnection)
    """
        prints the neurone
    """
    def printNeurone(self):
        print("\t id:", self.neuroneId, "name:", self.name, " type:", self.type)
        if self.outputConnection != [None] and not self.isChecked:
            self.isChecked = True
            for connection in self.outputConnection:
                if connection is not None and connection.enabled:
                    connection.printConnection()
    """
        feed forwards the informations
    """
    def feedForward(self, value):
        if self.type != "OUTPUT" and not self.isChecked:
            self.isChecked = True
            for connection in self.outputConnection:
                if connection is not None and connection.enabled:
                    connection.output.feedForward(value * connection.weight)
        elif self.type == "OUTPUT":
            self.value += value

    """
        Builds topology from this neurone
    """
    def getTopology(self, infoPath, topology):
        isEndOfPath = True
        #prevents infinite loops
        if not self.isChecked:
            self.isChecked = True
            if self.type == "INPUT":
                infoPath.append(self.neuroneId)
            for connection in self.outputConnection:
                if connection is not None and connection.enabled:
                    isEndOfPath = False
                    infoPath.append(connection.weight)
                    connection.output.getTopology(infoPath, topology)
        else:
            isEndOfPath = True
        if isEndOfPath:
            infoPath.append(self.neuroneId)
            topology.append(infoPath)


class Connection:
    def __init__(self, outputNeurone, genome, weight=random.uniform(-2, 2)):
        self.output = outputNeurone
        self.weight = weight
        self.enabled = True
        self.innovationNumber = genome.innovationNumber
        self.genome = genome
        self.output.inputSize += 1
        self.genome.innovationNumber += 1

    """
        Randomly modify the weight of a connection
    """

    def pointMutate(self):
        weightMutationFactor = random.uniform(-0.25, 0.25)
        if random.random() < 0.9:
            self.weight += weightMutationFactor
        else:
            self.weight = random.uniform(-2, 2)

    """
        Add a new neurone in the genome and modify the existing connection
    """

    def neuroneMutate(self):
        self.output.inputSize -= 1
        newConnection = Connection(self.output, self.genome, weight=self.weight)
        self.output = Neurone(self.output.genome, newConnection)
        self.output.inputSize += 1
        self.weight = 1

    """
        Enable or disables connection
    """

    def enableDisableMutate(self):
        self.enabled = not self.enabled
        if self.enabled:
            self.output.inputSize += 1
        else:
            self.output.inputSize -= 1

    def printConnection(self):
        print("\t weight :" + str(self.weight))
        self.output.printNeurone()
