# Install
1. required python version : 3.6 __be careful and always install dependencies to the 3.6 version of python__
2. pygame :
    * install dependencies
    sudo apt install mercurial python3-dev python3-setuptools python3-numpy python3-opengl \
        libav-tools libsdl-image1.2-dev libsdl-mixer1.2-dev libsdl-ttf2.0-dev libsmpeg-dev \
        libsdl1.2-dev libportmidi-dev libswscale-dev libavformat-dev libavcodec-dev \
        libtiff5-dev libx11-6 libx11-dev fluid-soundfont-gm timgm6mb-soundfont \
        xfonts-base xfonts-100dpi xfonts-75dpi xfonts-cyrillic fontconfig fonts-freefont-ttf
    * Grab source
    hg clone https://bitbucket.org/pygame/pygame
    * Finally build and install
    cd pygame
    python3 setup.py build
    sudo python3 setup.py install
    * sudo apt install python3-tk

3. python learning environment: http://pygame-learning-environment.readthedocs.io/en/latest/user/home.html#installation
4. Install
    * networkx
    * jsonpickle
    * matplotlib
    * Pillow
    
# Running
Run `python Main.py`

arguments:
* reinit = reinitialises the list of genomes
* save = saves the genomes to a savefile
* poolSize X = sets the pool size to X (default is 100)
