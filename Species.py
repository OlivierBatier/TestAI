class Species:
    speciesId = 0

    def __init__(self):
        self.id = Species.speciesId
        Species.speciesId += 1
        self.genomes = []

    def addGenome(self, genome):
        if genome not in self.genomes:
            self.genomes.append(genome)
            genome.speciesId = self.id
        else:
            self.genomes.append(genome)
            genome.genomeId += 1
            genome.id = genome.genomeId
            genome.speciesId = self.id

    def printSpecies(self):
        print("Species: " + str(self.id))
        for genome in self.genomes:
            genome.printGenome()

    def getSpeciesFitness(self):
        totalFitness = 0
        for genome in self.genomes:
            totalFitness += genome.fitness
        return totalFitness
